<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        body {font-family: Arial;}

        /* Style the tab */
        .tab {
            overflow: hidden;
            border: 1px solid #ccc;
            background-color: #f1f1f1;
        }

        /* Style the buttons inside the tab */
        .tab button {
            background-color: inherit;
            float: left;
            border: none;
            outline: none;
            cursor: pointer;
            padding: 14px 16px;
            transition: 0.3s;
            font-size: 17px;
        }

        /* Change background color of buttons on hover */
        .tab button:hover {
            background-color: #ddd;
        }

        /* Create an active/current tablink class */
        .tab button.active {
            background-color: #ccc;
        }

        /* Style the tab content */
        .tabcontent {
            display: none;
            padding: 6px 12px;
            border: 1px solid #ccc;
            border-top: none;
        }

        input[type=text], select {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }

        input[type=submit] {
            width: 100%;
            background-color: #4CAF50;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        input[type=submit]:hover {
            background-color: #45a049;
        }

        div {
            border-radius: 5px;
            background-color: #f2f2f2;
            padding: 20px;
        }

        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        th{
            background: #0BB32D;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
        .button {
            background-color: #4CAF50;
            border: none;
            color: white;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
        }
    </style>
</head>
<body>
    <div class="tab">
        @yield("cabecera")
        <button class="tablinks" onclick="openCity(event, 'Insertar')">Nuevo</button>
        <button class="tablinks" onclick="openCity(event, 'Actualizar')">Actualizar</button>
        <button class="tablinks" onclick="openCity(event, 'Tabla')">Tabla</button>
    </div>

    <div id="Insertar" class="tabcontent">
        @yield("registro")
        <form action="/insertar" method="POST" target='blank'>
            @csrf

            <label for="fname">Nombre</label>
            <input type="text" id="fname" name="nombre"><br><br>
      
            <label for="country">Edad</label>
            <input type="number" id="lname" name="edad"><br><br><br>

            <label for="lname">Afiliacion</label>
            <input type="text" id="lname" name="afiliacion"><br>

            <label for="lname">Rango</label>
            <input type="text" id="lname" name="rango"><br>
      
            <label for="country">Genero</label><br>
                <select id="country" name="genero">
                    <option value="mujer">Mujer</option>
                    <option value="hombre">Hombre</option>
                </select>
        
            <input type="submit" value="Submit">
        </form>
    </div>

    <div id="Actualizar" class="tabcontent">
        @yield("actualizar")
        <form action="/actualizar" method="POST" target='_blank'>
            @csrf   
            <label for="fname">Id</label>
            <input type="number" id="fname" name="id"><br><br>

            <label for="fname">Nombre</label>
            <input type="text" id="fname" name="nombre"><br><br>
      
            <label for="country">Edad</label>
            <input type="number" id="lname" name="edad"><br><br><br>

            <label for="lname">Afiliacion</label>
            <input type="text" id="lname" name="afiliacion"><br>

            <label for="lname">Rango</label>
            <input type="text" id="lname" name="rango"><br>
      
            <label for="country">Genero</label><br>
            <select id="country" name="genero">
                <option value="mujer">Mujer</option>
                <option value="hombre">Hombre</option>
            </select>
            <input type="submit" value="Submit">
        </form>   
    </div>

    <div id="Tabla" class="tabcontent">
        @yield("tabla")
            <table>
                <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Edad</th>
                    <th>Rango</th>
                    <th>Afiliacion</th>
                    <th>Genero</th>
                    <th></th>
                </tr>
                @foreach( $consulta as $celda )
                    <tr>
                        <td> {{ $celda->id }} </td>
                        <td>{{ $celda->nombre }}</td>
                        <td> {{ $celda->edad }} </td>
                        <td>{{ $celda->rango }}</td>
                        <td>{{ $celda->afiliacion }}</td>
                        <td>{{ $celda->genero }}</td>
                        <td>
                        <button onclick="eliminar(event, 'Eliminar', {{ $celda->id }} )">Eliminar</button>
                        </td>
                    </tr>

                @endforeach

            </table>
    </div>

    <div id="Eliminar" class="tabcontent">
        @yield("eliminar")
        <form action="/eliminar" method="POST" target="_blank">
            @csrf
            <label for="fname">Id</label>
            <input type="number" id="idFormActualizar" name="id" ><br><br>
            <input class="button" type="submit" value="Eliminar">
        </form>
    </div>

    <script>
        function openCity(evt, cityName) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }
        function eliminar(evt, cityName, id){
            openCity(evt,cityName);
            console.log(id);
            document.getElementById("idFormActualizar").value=id;

        }
    </script>
    
</body>
</html>