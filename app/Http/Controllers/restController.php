<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Personaje;

class restController extends Controller
{
    //
    public function index(){
        $datos=Personaje::all();
        //$array=json_decode($datos);
        $arrays=json_encode($datos);

        return view('crud',['consulta'=>$datos]);
        //return $datos;
    }
    public function consulta(){
        $personaje=Personaje::all();
        return $personaje;
    }

    public function insertar(Request $request){
        $personaje=new Personaje;
        try {
            $personaje->nombre=$request->nombre;
            $personaje->edad=$request->edad;
            $personaje->rango=$request->rango;
            $personaje->afiliacion=$request->afiliacion;
            $personaje->genero=$request->genero;
            $personaje->save();
        } catch (\Throwable $th) {
            throw $th;
        }
        return $personaje;
    }

    public function actualizar(Request $request){
        $personaje=Personaje::findOrFail($request->id);
        try {
            $personaje->nombre=$request->nombre;
            $personaje->edad=$request->edad;
            $personaje->rango=$request->rango;
            $personaje->afiliacion=$request->afiliacion;
            $personaje->genero=$request->genero;
            $personaje->save();            
        } catch (\Throwable $th) {
            throw $th;
        }
        return $personaje;
    }

    public function eliminar(Request $request){
        $personaje=Personaje::findOrFail($request->id);
        try {
            $personaje->delete();
        } catch (\Throwable $th) {
            throw $th;
        }
        return $request->id;
    }

    
    
}
