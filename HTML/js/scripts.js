function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
  }
  
  function obtenerDatos(){
      const url="http://localhost:8000/consulta";
      let variable=fetch(url)
      .then(function(response) {
          return response.json();
      })
      .then(function(personajes) {
        
          /****************************************** */
        //eliminar tabla al presionar boton si es que existe 
        if(document.getElementById("tabla")){
          let eliminar=document.getElementById("tabla");
          var padre = eliminar.parentNode;
		      padre.removeChild(eliminar);
        }
              // Obtener la referencia del elemento body
            //var body = document.getElementsByTagName("body")[0];
        var body=document.getElementById("Tabla");
   
        // Crea un elemento <table> y un elemento <tbody>
        var tabla   = document.createElement("table");
        var tblBody = document.createElement("tbody");
        var filas=personajes.length;
   
        // Crea las celdas
        for (var i = 0; i < filas; i++) {
        // Crea las hileras de la tabla
          var hilera = document.createElement("tr");
   
          //for (var j = 0; j < columnas; j++) {
          // Crea un elemento <td> y un nodo de texto, haz que el nodo de
          // texto sea el contenido de <td>, ubica el elemento <td> al final
          // de la hilera de la tabla
          var celda = document.createElement("td");
          var textoCelda = document.createTextNode(personajes[i].nombre);
          celda.appendChild(textoCelda);
          hilera.appendChild(celda);
          //edad
          var celda = document.createElement("td");
          var textoCelda = document.createTextNode(personajes[i].edad);
          celda.appendChild(textoCelda);
          hilera.appendChild(celda);
          //rango
          var celda = document.createElement("td");
          var textoCelda = document.createTextNode(personajes[i].rango);
          celda.appendChild(textoCelda);
          hilera.appendChild(celda);
          //afiliacion
          var celda = document.createElement("td");
          var textoCelda = document.createTextNode(personajes[i].afiliacion);
          celda.appendChild(textoCelda);
          hilera.appendChild(celda);
          //genero
          var celda = document.createElement("td");
          var textoCelda = document.createTextNode(personajes[i].genero);
          celda.appendChild(textoCelda);
          hilera.appendChild(celda);

          //boton
          var celda = document.createElement("button");
          var textoCelda = document.createTextNode("Eliminar");
          celda.appendChild(textoCelda);
          hilera.appendChild(celda);
          celda.setAttribute("class","button");
          celda.setAttribute('onclick',`
          var url = 'http://127.0.0.1:8000/eliminar';
          var data = {id: ${personajes[i].id}};

          fetch(url, {
            method: 'POST', // or 'PUT'
            body: JSON.stringify(data), // data can be string or {object}!
            headers:{
                'Content-Type': 'application/json'
            }
          }).then(res => res.json())
          .catch(error => console.error('Error:', error))
          .then(response => console.log('Success:', response));
        
          `);

        //}
   
          // agrega la hilera al final de la tabla (al final del elemento tblbody)
        tblBody.appendChild(hilera);
      }
   
      // posiciona el <tbody> debajo del elemento <table>
      tabla.appendChild(tblBody);
      tabla.setAttribute('id',"tabla");
      // appends <table> into <body>
      body.appendChild(tabla);
      //document.body.insertBefore(body,getElementById("Tabla"));

          /************************************************* */
    });
      console.log(variable);
      //generar_tabla(personajes.length,personajes);
  }

  function genera_tabla(filas,personajes) {
    // Obtener la referencia del elemento body
    var body = document.getElementsByTagName("body")[0];
   
    // Crea un elemento <table> y un elemento <tbody>
    var tabla   = document.createElement("table");
    var tblBody = document.createElement("tbody");
   
    // Crea las celdas
    for (var i = 0; i < filas; i++) {
      // Crea las hileras de la tabla
      var hilera = document.createElement("tr");
   
      //for (var j = 0; j < columnas; j++) {
        // Crea un elemento <td> y un nodo de texto, haz que el nodo de
        // texto sea el contenido de <td>, ubica el elemento <td> al final
        // de la hilera de la tabla
        var celda = document.createElement("td");
        var textoCelda = document.createTextNode(personajes[i].nombre);
        celda.appendChild(textoCelda);
        hilera.appendChild(celda);
        //edad
        var celda = document.createElement("td");
        var textoCelda = document.createTextNode(personajes[i].edad);
        celda.appendChild(textoCelda);
        hilera.appendChild(celda);
        //rango
        var celda = document.createElement("td");
        var textoCelda = document.createTextNode(personajes[i].rango);
        celda.appendChild(textoCelda);
        hilera.appendChild(celda);
        //afiliacion
        var celda = document.createElement("td");
        var textoCelda = document.createTextNode(personajes[i].afiliacion);
        celda.appendChild(textoCelda);
        hilera.appendChild(celda);
        //genero
        var celda = document.createElement("td");
        var textoCelda = document.createTextNode(personajes[i].genero);
        celda.appendChild(textoCelda);
        hilera.appendChild(celda);
      //}
   
      // agrega la hilera al final de la tabla (al final del elemento tblbody)
      tblBody.appendChild(hilera);
    }
   
    // posiciona el <tbody> debajo del elemento <table>
    tabla.appendChild(tblBody);
    // appends <table> into <body>
    body.appendChild(tabla);
  }

  