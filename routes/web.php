<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','App\Http\Controllers\restController@index');
Route::get('consulta','App\Http\Controllers\restController@consulta');
Route::post('insertar','App\Http\Controllers\restController@insertar');
Route::post('actualizar','App\Http\Controllers\restController@actualizar');
Route::post('eliminar','App\Http\Controllers\restController@eliminar');